# Send analog sensor voltages to an e-mail when a digital pin is triggered.

## Components

- NodeMCU V1.0 board

    https://tronixlabs.com.au/wireless/esp8266/nodemcu-lua-based-esp8266-development-kit-australia/

- MCP3008 8-Channel 10-Bit ADC with SPI Interface

    https://core-electronics.com.au/mcp3008-8-channel-10-bit-adc-with-spi-interface.html

## Connection Guide

MCP3008 | NodeMCU
:---: | :---:
VDD | 3V3
VREF | 3V3
AGND | GND | GND | GND | GND
CLK | D5
DOUT | D6
DIN | D7
CS | D8
DGND | GND

Preference: https://www.youtube.com/watch?v=NGNNDz_ylzs

## How To

1. Installing Visual Studio Code. (Skip this if you had already installed on your PC)

- Download and install Visual Studio Code from [here](https://code.visualstudio.com/download)

- Install **PlatformIO IDE** by following [this](http://docs.platformio.org/en/latest/ide/vscode.html#installation) link.

2. Download the source code from the gitlab repo.

- Visit https://gitlab.com/rpiguru/esp8266-gmail-mc3008

- Download the source code in zip format and extract all files to your hard drive.

3. Install the USB driver of the NodeMCU. (Skip this if you had already installed the driver on your PC)

    In default, Windows does not support the USB driver of the NodeMCU.
    
    Follow this link - https://cityos-air.readme.io/docs/1-usb-drivers-for-nodemcu-v10#section-13-nodemcu-v10-driver

4. Programme the NodeMCU.

- Open the **VS Code**.

- `File` -> `Open Folder`, and select the directory you had downloaded and extracted.

- Open `lib/Gsender.h` by double clicking it, and fill some values in the source code.
    * `const char* EMAILBASE64_LOGIN = "";`
    * `const char* EMAILBASE64_PASSWORD = "";`
    * `const char* FROM = "***your_email***@gmail.com";`
    (You can use [base64encode.org](https://base64encode.org) for encoding)

- Open `main.cpp` and change some values in the code.

        const char* ssid = "";                              // WIFI network name
        const char* password = "";                          // WIFI network password
        String subject = "This is Subject.";
        const String to_mail = "rpiguru@techie.com";
        const String message = "This is the mail from ESP8266";
        static const uint8_t INPUT_PIN = D1;                // Input pin to be used

- Press `Alt+Ctrl+B` to build the source code.

- Press `Alt+Ctrl+U` to upload to the NodeMCU.

- Press `Alt+Ctrl+S` to open the serial monitor and see how it works.
    
    *NOTE:* NodeMCU's blue LED will blink twice in a second when it tried to connect to the RPi thru wifi.
