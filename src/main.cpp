#include <Arduino.h>
#include "Gsender.h"
#include <ESP8266WiFi.h>
#include <Adafruit_MCP3008.h>


// ============================== Change values here ==============================
const char* ssid = "";                              // WIFI network name
const char* password = "";                          // WIFI network password
String subject = "Machine Data";
const String to_mail = "rpiguru@techie.com";
static const uint8_t INPUT_PIN = D1;                // Input pin to be used


uint8_t connection_state = 0;                       // Connected to WIFI or not
uint16_t reconnect_interval = 10000;                // If not connected wait time to try again(ms)
Gsender *gsender = Gsender::Instance();             // Getting pointer to class instance
Adafruit_MCP3008 adc;
unsigned long last_change_time = millis();
bool input_changed = false;


void setup_wifi() {

  int attempt = 0;
  String macAddr;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}

void on_input_change(){
    if (millis() - last_change_time > 200){
        last_change_time = millis();
        input_changed = true;
        Serial.println("Input is changed!");
    }
}


void setup()
{
  Serial.begin(9600);

  pinMode(D0, OUTPUT);
  pinMode(INPUT_PIN, INPUT);

  setup_wifi();

  adc.begin();

  attachInterrupt(INPUT_PIN, on_input_change, RISING);

  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);
}

void send_email(){
    String msg = "Sensor Data: \n";
    char* buf = "";
    
    dtostrf((float)adc.readADC(0) * 3.3 / 1024, 5, 3, buf);
    msg = msg + "pH: " + buf + "\n";

    dtostrf((float)adc.readADC(1) * 3.3 / 1024, 5, 3, buf);
    msg = msg + "ORP: " + buf + "\n";

    dtostrf((float)adc.readADC(2) * 3.3 / 1024, 5, 3, buf);
    msg = msg + "cl: " + buf + "\n";

    dtostrf((float)adc.readADC(3) * 3.3 / 1024, 5, 3, buf);
    msg = msg + "Temp: " + buf + "\n";

    dtostrf((float)adc.readADC(4) * 3.3 / 1024, 5, 3, buf);
    msg = msg + "Spare1: " + buf + "\n";

    Serial.print("Body:");
    Serial.println(msg);
    if(gsender->Subject(subject)->Send(to_mail, msg)) {
        Serial.println("e-mail sent.");
    } else {
        Serial.print("Error sending e-mail: ");
        Serial.println(gsender->getError());
    }
}

void loop(){
    if (digitalRead(INPUT_PIN)){
        if (input_changed){
            Serial.println("Sending email now");
            send_email();
            input_changed = false;
        }
    }
    else {
        input_changed = false;
    }

    delay(100);
}
